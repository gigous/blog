# Linux Restart GUI

If the screen is frozen but system otherwise responsive, these steps may help.

**NOTE**: Personally tested only on Ubuntu 18.04+

## Solution 1

1. Where is the screen frozen? Go there if you aren't already.
  - (ex: tty 2 on Ubuntu, <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F2</kbd>)
  - <kbd>Host Key</kbd>+<kbd>F2</kbd> if in VirtualBox VM
2. Type <kbd>Alt</kbd>+<kbd>F2</kbd>, then type <kbd>r</kbd> into prompt, then <kbd>Enter</kbd>
3. The screen should "refresh" after a bit

## Solution 2

Is Solution 1 not working or not displaying the prompt?

1. Go to another TTY
  - Example: <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>F4</kbd> for TTY4
2. Login
3. Set `$DISPLAY` to `:0.0`
  - `export DISPLAY=:0.0`
4. Enter command `xdotool key "Alt+F2+r" && sleep 0.5 && xdotool key "Return"`
  - Install xdotool if needed
5. See if it worked by going back to your GUI session thing
6. (Optional) Make an alias for steps 3 and 4


## Sources

[How to execute Alt + F2 + r + Return (Gnome Shell Reset) through the Terminal](https://web.archive.org/web/20200512100755/https://askubuntu.com/questions/1159737/how-to-execute-alt-f2-r-return-gnome-shell-reset-through-the-terminal)
[$DISPLAY environment variable](https://web.archive.org/web/20210225060315/https://askubuntu.com/questions/432255/what-is-the-display-environment-variable)



