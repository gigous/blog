# Autotype for screencasts in MacOS

You want to not make mistakes and get typing done more quickly? Then do this.

## AppleScript To Type Text In Clipboard

Use the second code block [here](https://softwarerecs.stackexchange.com/a/10055).

## Activate Another App

Use [this](https://stackoverflow.com/questions/4996334/send-a-key-code-to-an-application-without-activating-it-first/16504528#16504528) to activate whatever app you want to autotype into. Put the activate statement inside the `tell` block for System Events.

## Run AppleScript As Application

Convert an AppleScript to app using [these](https://www.addictivetips.com/mac-os/convert-an-applescript-to-an-app-on-macos/) instructions.

DO NOT check "Show startup screen" in save as dialog.

## Run AppleScript With Keyboard Shortcut

Another thanks to [Addictive Tips](https://www.addictivetips.com/mac-os/run-an-applescript-with-a-keyboard-shortcut-on-macos/)

## If Typing Into VSCode

You might want to [disable autoIndent](https://github.com/microsoft/vscode/issues/5446) to make your life easier.

```json
{
    "editor.autoIndent": "none"
}
```

## The Finished Product

```applescript
try
    set getClip to the clipboard as text
on error
    set getClip to " "
end try

--set asc to 0

tell application "System Events"
    tell application "Visual Studio Code - Insiders" to activate
    repeat with i from 1 to count characters of getClip
        -- You can use ASCII number <text> to get the
        -- ASCII value of a character
        --set asc to ASCII number (character i of getClip)
        
        keystroke (character i of getClip)
        
        delay 0.05
    end repeat
end tell
```
