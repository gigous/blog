# Notepad Create Macro

Kinda self-explanatory but whatever.

## Steps

1. Click Start Recording button
  - Icon with the red circle in a box, in toolbar
2. Type what you want the macro to do
  - *Including* arrow keys to place the cursor in the ideal spot
  - Example: For Markdown, typing `<kbd></kbd>` then 6 arrow left keypresses would place cursor in middle of tags
3. Click Stop Recording button
  - Black square in box in toolbar
4. Click Save Current Macro button
  - weird looking icon at end of macro toolbar button group thing
5. Make a name and a shortcut
  - ideally that doesn't conflict with anything else
6. Obviously, try it out

## If You Read This Far

I appreciate you.